# Answer Digital Technical Test Standard

## Installation

1. Clone the repository: `git clone https://peacedude@bitbucket.org/peacedude/answerdigitaldotnetcore.git`
2. If you have visual studio 2017 or later installed open the solution (`.sln`)
3. Build the solution
4. Open the Test Explorer window to run tests

Alternatively the test can be run using the Nunit console runner

## Setting Driver location
By Default ChromeDriver,InternetExplorerDriver and geckoDriver are installed using Nuget. If you have drivers already installed, then paths to the directories containing the executables can be specified by copying `sample.env` into a file called `.env` and paths entered in the appropriate variables

## Known issues:

- Internet Explorer driver fails rather than implicitly scroll using `actions.moveTo()`
- Internet Explorer driver strips exclamation marks from strings when asserting the content of error messages on form authentication page
- This behavior is also seen using chrome driver on linux(Ubuntu)

These are still being investigated post-submission.

## Contact
If you have any more  questions contact me at mulcaheygeorge@gmail.com
