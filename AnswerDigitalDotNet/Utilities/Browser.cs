﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerDigitalDotNet.Utilities
{
    class Browser : IDisposable
    {
        IWebDriver driver;
        OpenQA.Selenium.Support.UI.WebDriverWait wait;

        public Browser(string browsername = "")
        {            
            browsername = browsername.ToLower();
            switch (browsername)
            {
                case "firefox":
                    if (Environment.GetEnvironmentVariable("FIREFOX_DRIVER") != null) driver = new FirefoxDriver(Environment
                        .GetEnvironmentVariable("FIREFOX_DRIVER"));
                    else driver = new FirefoxDriver(Environment.CurrentDirectory);
                    break;
                case "ie":
                    if (Environment.GetEnvironmentVariable("IE_DRIVER") != null) driver = new InternetExplorerDriver(Environment
                        .GetEnvironmentVariable("IE_DRIVER"));
                    else driver = new InternetExplorerDriver(Environment.CurrentDirectory);
                    break;
                default:
                    if (Environment.GetEnvironmentVariable("CHROME_DRIVER") != null) driver = new ChromeDriver(Environment
                        .GetEnvironmentVariable("CHROME_DRIVER"));
                    else driver = new ChromeDriver(Environment.CurrentDirectory);
                    break;
            }

            wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(20));
        }

        public void Dispose()
        {
            driver.Close();
            driver.Quit();
            driver = null;
            
        }

        public IWebDriver Drive()
        {
            return driver;
        }

        public OpenQA.Selenium.Support.UI.WebDriverWait Wait()
        {
            return wait;
        }
    }
}
