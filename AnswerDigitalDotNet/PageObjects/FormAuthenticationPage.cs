﻿using AnswerDigitalDotNet.Utilities;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerDigitalDotNet.PageObjects
{
    class FormAuthenticationPage
    {
        public static IWebElement UsernameField(Browser browser)
        {
            return browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.Id("username")));
        }

        public static IWebElement PasswordField(Browser browser)
        {
            return browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.Id("password")));
        }

        public static IWebElement LoginButton(Browser browser)
        {
            return browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.XPath("//form[@id='login']//button[@type='submit']")));
        }

        public static string OutcomeText(Browser browser)
        {
            string result = browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.Id("flash"))).Text;

            //remove newline and carriage return characters from string before returning -- consider being explicit
            return result.Remove(result.Length - 3);
        }
    }
}
