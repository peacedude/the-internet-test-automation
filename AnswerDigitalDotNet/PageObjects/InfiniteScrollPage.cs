﻿using AnswerDigitalDotNet.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerDigitalDotNet.PageObjects
{
    class InfiniteScrollPage
    {
        public static void ScrolltoElement(Browser browser, IWebElement element)
        {
            Actions actions = new Actions(browser.Drive());
            actions.MoveToElement(element);
            actions.Perform();

        }

        public static void ScrollToFooter(Browser browser)
        {
            IWebElement footer = browser.Wait().Until(ExpectedConditions
                .ElementExists(By.LinkText("Elemental Selenium")));

            ScrolltoElement(browser, footer);
            
        }

        public static void ScrollToFooter(Browser browser, int count)
        {
            count++; // first scroll attempt doesn't scroll at all
            for (int i = 0; i < count; i++) ScrollToFooter(browser);
        }

        public static void ScrollToTop(Browser browser)
        {
            ScrolltoElement(browser, PageHeading(browser));
            

        }

        public static IWebElement PageHeading(Browser browser)
        {
            return browser.Wait().Until(ExpectedConditions.ElementExists(By.TagName("h3")));
        }

       
    }
}
