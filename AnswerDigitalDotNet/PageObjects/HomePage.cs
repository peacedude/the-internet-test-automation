﻿using AnswerDigitalDotNet.Utilities;
using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerDigitalDotNet.PageObjects
{
    class HomePage
    {
        //only menu items required for the current Test include. 
        //May be expanded as attempted test coverage increases.

        public static IWebElement FormAuthenticationLink(Browser browser) {
            return browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Form Authentication")));
        }

        public static IWebElement InfiniteScrollLink(Browser browser)
        {
            return browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Infinite Scroll")));
        }

        public static IWebElement KeypressLink(Browser browser)
        {
            return browser.Wait().Until(ExpectedConditions.ElementIsVisible(By.LinkText("Key Presses")));
        }
    }
}
