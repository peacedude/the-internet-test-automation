using AnswerDigitalDotNet.PageObjects;
using AnswerDigitalDotNet.Utilities;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tests
{
    public class Tests
    {
        Browser browser;
        Dictionary<string, string> TestData;
        string TestKeys;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            DotNetEnv.Env.Load();
            browser = new Browser();
            TestData = new Dictionary<string, string>();
            TestData["IncorrectPassword"] = "JustPlainWrong";
            TestData["IncorrectUsername"] = "WrongUsername";
            TestData["CorrectPassword"] = "SuperSecretPassword!";
            TestData["CorrectUsername"] = "tomsmith";
            //no Lock keys(Caps Lock, Num Lock, Scroll Lock) are supported
            //all alphabetical characters must be capitals
            TestKeys = "AZ" + Keys.Right + Keys.NumberPad5;


        }

        [SetUp]
        public void Setup()
        {
            browser.Drive().Manage().Window.Maximize();
            browser.Drive().Url = "http://the-internet.herokuapp.com/";
        }
        //form authentication correct username and wrong password
        [Test]
        public void LoginWithCorrectUsernameandIncorrectPassword()
        {
            //arrange
            HomePage.FormAuthenticationLink(browser).Click();

            FormAuthenticationPage.UsernameField(browser).SendKeys(TestData["CorrectUsername"]);
            FormAuthenticationPage.PasswordField(browser).SendKeys(TestData["IncorrectPassword"]);

            //act
            FormAuthenticationPage.LoginButton(browser).Click();

            //assert
            Assert.AreEqual("Your password is invalid!",FormAuthenticationPage.OutcomeText(browser));
        }
        //form authentication incorrect username and correct password
        [Test]
        public void LoginWithIncorrectUsernameAndCorrectPassword()
        {
            //arrange
            HomePage.FormAuthenticationLink(browser).Click();

            FormAuthenticationPage.UsernameField(browser).SendKeys(TestData["IncorrectUsername"]);
            FormAuthenticationPage.PasswordField(browser).SendKeys(TestData["CorrectPassword"]);

            //act
            FormAuthenticationPage.LoginButton(browser).Click();

            //assert
            Assert.AreEqual("Your username is invalid!", FormAuthenticationPage.OutcomeText(browser));

        }
        // form authentication correct username and password
        [Test]
        public void LoginWithCorrectUsernameAndPassword()
        {
            //arrange
            HomePage.FormAuthenticationLink(browser).Click();

            FormAuthenticationPage.UsernameField(browser).SendKeys(TestData["CorrectUsername"]);
            FormAuthenticationPage.PasswordField(browser).SendKeys(TestData["CorrectPassword"]);

            //act
            FormAuthenticationPage.LoginButton(browser).Click();

            //assert
            Assert.AreEqual("You logged into a secure area!", FormAuthenticationPage.OutcomeText(browser));
        }
        //Infinite Scroll
        [Test]
        public void InfiniteScroll()
        {
            //arrange
            HomePage.InfiniteScrollLink(browser).Click();

            //act
            InfiniteScrollPage.ScrollToFooter(browser, 2);
            InfiniteScrollPage.ScrollToTop(browser);

            //assert
            Assert.IsTrue(InfiniteScrollPage.PageHeading(browser).Displayed);
            Assert.AreEqual("Infinite Scroll", InfiniteScrollPage.PageHeading(browser).Text);
        }
        //keypress click 4 keys and assert text displayed
        [Test]
        public void KeyPresses()
        {
            //arrange
            HomePage.KeypressLink(browser).Click();

            for(int i = 0; i < TestKeys.Length; i++) {
                //Act
                KeypressPage.wholePage(browser).SendKeys(TestKeys[i].ToString());
                
                //Assert
                Assert.AreEqual(TestKeys[i].ToString(), KeypressPage.DisplayedKey(browser));
                                
            }
            
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            browser.Dispose();
        }
    }
}